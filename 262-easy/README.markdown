Challenge #262 [Easy] MaybeNumeric
==================================

https://www.reddit.com/r/dailyprogrammer/comments/4eaeff/20160411_challenge_262_easy_maybenumeric/

MaybeNumeric is a function that returns either a number or a string depending on whether the input (string) is a valid description of a number.


bonus 2: parsing separated values

(clarification: backtick is the sparator. space is only a separator for numeric arrays)
 2015 4 4`Challenge #`261`Easy
 234.2`234ggf 45`00`number string number (0)
