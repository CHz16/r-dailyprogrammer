//: Playground - noun: a place where people can play

import Cocoa


// Dead simple implementation of a tagged union with a common constructor.
enum MaybeNumeric {
    case Num(Double), Str(String)

    init(_ s: String) {
        if let n = Double(s) {
            self = .Num(n)
        } else {
            self = .Str(s)
        }
    }
}

MaybeNumeric("123")
MaybeNumeric("44.234")
MaybeNumeric("0x123N")


// Bonus 2

func parseSeparatedValues(_ s: String) -> [MaybeNumeric] {
    // A constructor is just another function, so you can use it as the transformation function in map and that's totally cool.
    return s.components(separatedBy: "`").map(MaybeNumeric.init)
}

parseSeparatedValues("2015 4 4`Challenge #`261`Easy")
parseSeparatedValues("234.2`234ggf 45`00`number string number (0)")
