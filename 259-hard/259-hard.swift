#!/usr/bin/env xcrun swift

import Cocoa


// As a reminder: the order of operations here is entirely left to right, so 0 + 1 + 2 * 3 = 9, not 7. As a convention, I'll quote operator strings in `backticks`, so `10020` = 21.
// Definition: the best possible representation `R` of length L of a number N is a string of length L in operator format that meets both of the rule 5 tiebreakers: it has the most `0`s of any string of length L representing N, and if there is more than one possible representation with the most `0`s, it's the first in lexicographic order.
// Finding the best possible representation has optimal substructure: if `AB` is the best possible representation of length L of a number N, where `B` is the last character (`0`, `1`, or `2`) and `A` is a string of length L - 1 representing a number M, then `A` is the *best* possible representation of length L - 1 of M. We can therefore find N's best possible representation of length L by finding strings `X`, `Y`, and `Z` such that `X0` = `Y1` = `Z2` = N, where `X`, `Y`, and `Z` are the best possible representations of length L - 1 of the numbers they represent, and then determining which of `X0`, `Y1`, and `Z2` is the best possible representation of N.
// It is possible that one or more of the strings `X`, `Y`, and `Z` don't exist, and if none exist, then there is no representation of N of length L.
//
// Proof of optimal substructure:
// Let `AB` be the best possible representation of length L of N, where `B` is the last character and `A` is a string of length L - 1 representing a number M. We will assume L > 1 so `A` is not an empty string; there are only three strings of length 1: `0` = 1, `1` = -1, and `2` = 0, all of which are trivially the best representations of length 1 of the numbers they represent.
// We will show that `A` is the best representation of length L - 1 of M by contradiction.
// Assume that `A` is not the best representation of length L - 1 of M. Then, there exists an `A'` different from `A` that *is* the best representation of length L - 1 of M. `A'` = M = `A` by definition.
// Consider the string `A'B`. There are three cases to consider:
//  - `B` is `0`: `A'B` = `A'0` = `A'` + L = `A` + L = `A0` = `AB` = N
//  - `B` is `1`: `A'B` = `A'` - L = `A` - L = `AB` = N
//  - `B` is `2`: `A'B` = `A'` * L = `A` * L = `AB` = N
// `A'B` = N in all possible cases, so `A'B` is also a representation of length L of N.
// Since `A'` is a better representation of M than `A`, it must either contain more zeroes than `A`, or it must contain the same number of zeroes but fall earlier lexicographically than `A`. If `A'` has more zeroes than `A`, then `A'B` has more zeroes than `AB`, so `A'B` is a better representation of N than `AB`. If `A'` has the same number of zeroes as `A` but falls earlier lexicographically than it, then `A'B` has the same number of zeroes as `AB` but falls earlier lexicographically than it, so `A'B` is again a better representation of N than `AB`.
// However, by definition, `AB` is the best possible representation of length L of N, so this is a contradiction. Therefore, our assumption that `A` is not the best representation of length L - 1 of M is false, and `A` must be the best representation of length L - 1 of M. This proves the optimal substructure property asserted earlier.
//
// Thus, to find the best possible representation of length L of a number N, it suffices to calculate the strings `X`, `Y`, and `Z` recursively and compare them to find which is the best. In typical dynamic programming fashion, we'll memoize the results of the subproblems to save time calculating them repeatedly.
// To find the best possible representation of N of the *smallest* length, as the puzzle specifies, we first try to find the best possible representation of length 1, and failing that length 2, and so on.


// Cache structure: cache[n][length] is the best possible representation of n of a given length if a representation exists, and nil if no representation of that length exists.
var cache: [Int: [Int: String?]] = [-1: [1: "1"],
                                    0: [1: "2"],
                                    1: [1: "0"]]

func toNOS(_ n: Int) -> String {
    // Recursive helper function that tries to find the best possible representation of n of length l.
    func findNOS(n: Int, length: Int) -> String? {
        // There are two different places here in findNOS that add a value to the cache, so this seemed convenient.
        func addToCache(n: Int, length: Int, s: String?) {
            if cache[n] == nil {
                cache[n] = [:]
            }
            cache[n]![length] = s
        }

        // If we've already solved this subproblem, then just return the answer we found earlier.
        if let s = cache[n]?[length] {
            return s
        }

        // Base case of the recursion: if we've run out of operators, then it's impossible.
        if length == 0 {
            return nil
        }

        // Recursion: the representation of n must be A0, B1, or C2 for some strings A, B, and C of length l - 1.
        //   - A represents the number n - l, because A0 = (n - l) + l = n
        //   - B represents the number n + l, because B1 = (n + l) - l = n
        //   - C represents the number n / l, because C2 = (n / l) * l = n
        // So our recursion consists of finding these representations A, B, and C, if they exist, and then evaluating which of A0, B1, and C2 is the best representation of n.
        var candidates: [String] = []
        if let s = findNOS(n: n - length, length: length - 1) {
            candidates.append(s + "0")
        }
        if let s = findNOS(n: n + length, length: length - 1) {
            candidates.append(s + "1")
        }
        if n % length == 0 {
            if let s = findNOS(n: n / length, length: length - 1) {
                candidates.append(s + "2")
            }
        }

        // If we can't find a representation, then mark in the cache that it's impossible and bail out.
        if candidates.isEmpty {
            addToCache(n: n, length: length, s: nil)
            return nil;
        }

        // If there's more than one candidate for best representation, then apply the tiebreakers.
        if (candidates.count > 1) {
            // Tiebreaker #2: lexicographic order
            candidates = candidates.sorted()

            // Tiebreaker #1: most zeroes in the representation. By sorting first, we ensure that if there are multiple representations with the most zeroes, we'll hit the one that wins tiebreaker #2 first.
            var mostZeroes = Int.min
            var bestCandidate = ""
            for candidate in candidates {
                var zeroes = 0
                for c in candidate.characters {
                    if c == "0" {
                        zeroes += 1
                    }
                }
                if zeroes > mostZeroes {
                    mostZeroes = zeroes
                    bestCandidate = candidate
                }
            }

            // We can smash the array because we'll return in a sec.
            candidates[0] = bestCandidate
        }

        // We have our best representation! We did it. We're the best.
        addToCache(n: n, length: length, s: candidates[0])
        return candidates[0]
    }

    // Try to find the best possible representation of n of length 1. If it's impossible, then try to find the best one of length 2. And so on until we have a solution, which will be the best possible representation that's the shortest possible length.
    var digits = 1
    while true {
        if let s = findNOS(n: n, length: digits) {
            return s
        }
        digits += 1
    }
}

for _ in 0..<5 {
    let n = Int(arc4random_uniform(UInt32.max))
    let date = Date()
    print(n, toNOS(n))
    print(-date.timeIntervalSinceNow)
    print("")
}
