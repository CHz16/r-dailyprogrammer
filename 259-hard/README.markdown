[2016-03-25] Challenge #259 [Hard] Operator number system
=========================================================

https://www.reddit.com/r/dailyprogrammer/comments/4bwibm/20160325_challenge_259_hard_operator_number_system/

1. Each digit in a number represents one of 3 operators: - 0: + 1: - 2: *

2. The length of the number (count of digits) limits the natural number sequence used. A 4 digit number means the operators are inserted into the sequence 0 _ 1 _ 2 _ 3 _ 4

3. Operators are inserted left to right, and there are no special precedence rules for * multiplication.

4. The encoding used should use the fewest number of digits/operators possible:

Possible encodings of the number 10 are:

0000 = 0 + 1 + 2 + 3 + 4
0220 = 0 + 1 * 2 * 3 + 4
02212 = 0 + 1 * 2 * 3 - 4 * 5

Only the first 2 representations satisfy the 4th rule of being the shortest possible:

optional 5th rule: As a tie break for "correct representation" use the representation with the most 0s (representing +), and optionally if still tied, use the representation that would sort first. ex: first above 0000 representation of 10 has the most 0's. These tie breakers are arbitrary, and you may use any tie breaking scheme you want.
