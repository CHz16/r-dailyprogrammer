[2016-03-30] Challenge #260 [Intermediate] Diagonal collision
=============================================================

https://www.reddit.com/r/dailyprogrammer/comments/4cktc3/20160330_challenge_260_intermediate_diagonal/

You have one rectangle composed of X*Y squares, with X being the width and Y being the height. You want to know how many squares you are going to collide if you were to draw a diagonal, meaning a line between the bottom-left edge and the top-right edge.
