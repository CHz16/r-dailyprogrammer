//: Playground - noun: a place where people can play

import Cocoa


// Calculate the number of squares the diagonal passes through by counting how many squares it goes through between height 0 and height 1, height 1 and height 2, or generally from height n to height n + 1.
// The slope of the diagonal is height / width, so we know the line passes through (n / slope, n) and ((n + 1) / slope, n + 1).
// So to find out how many horizontal squares that passes through, all we have to do is subtract the left x coordinate from the right one and round up.
func collisions(width: Int, height: Int) -> Int {
    var squares = 0

    for row in 0..<height {
        let start = floor(Double(width * row) / Double(height))
        let finish = Double(width * (row + 1)) / Double(height)
        squares += Int(ceil(finish - start))
    }

    return squares
}

collisions(width: 5, height: 2)
collisions(width: 9, height: 3)
collisions(width: 100, height: 101)
collisions(width: 168, height: 189)
collisions(width: 500, height: 225)
collisions(width: 156, height: 325)
