[2016-04-06] Challenge #261 [Intermediate] rearranged magic squares
===================================================================

https://www.reddit.com/r/dailyprogrammer/comments/4dmm44/20160406_challenge_261_intermediate_rearranged/

You will be given an NxN grid that is not a magic square, but whose rows can be rearranged to form a magic square. In this case, rearranging the rows means to put the rows (horizontal lines of numbers) in a different order, but within each row the numbers stay the same. So for instance, the top row can be swapped with the second row, but the numbers within each row cannot be moved to a different position horizontally, and the numbers that are on the same row as each other to begin with must remain on the same row as each other.

Write a function to find a magic square formed by rearranging the rows of the given grid.
