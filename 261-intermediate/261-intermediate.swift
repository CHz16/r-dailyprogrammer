#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "squares.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


func permuteIntoMagicSquare(_ square: [[Int]]) -> [[Int]] {
    let n = square.count

    // Precalculate some constants we'll use to prune off some branches.

    // If we're selecting row x, then lowerBounds[x] is the absolute minimum sum we can reach with how many rows we have left to choose:
    //  - last row: 1
    //  - second-to-last: 1 + 2
    //  - third-to-last: 1 + 2 + 3
    //  - etc.
    var lowerBounds: [Int] = []
    for i in 0..<n {
        lowerBounds.append((n - i) * (n - i + 1) / 2)
    }

    // upperbounds[x] is the absolute maximum sum we can reach with how many rows we have left to choose:
    //  - last row: n^2
    //  - second-to-last: n^2 + (n^2 - 1)
    //  - third-to-last: n^2 + (n^2 - 1) + (n^2 - 2)
    //  - etc.
    var upperBounds = [n * n]
    for i in 1..<n {
        upperBounds.insert(upperBounds[0] + n * n - i, at: 0)
    }


    // Recursive helper function to try out every permutation of rows until we find one that works. Basically DFS with some pruning.
    // I think there's a solid chance this would be faster if I were less clever and just iteratively tried every permutation though D:
    func selectMagicSquare(rows: [[Int]], x: Int, leftTarget: Int, rightTarget: Int) -> [[Int]]? {
        // Base case: if we're at the last row, then check if it results in a good magic square.
        if (rows.count == 1) {
            if rows[0][x] == leftTarget && rows[0][0] == rightTarget {
                return rows
            } else {
                return nil
            }
        }

        // Prune branches where it's mathematically impossible to reach one of the diagonal targets.
        if (leftTarget > upperBounds[x] || leftTarget < lowerBounds[x] || rightTarget > upperBounds[x] || rightTarget < lowerBounds[x]) {
            return nil
        }

        // Test out each row in turn by selecting it and checking if we can rearrange the remaining rows to make a good magic square.
        for i in 0..<rows.count {
            let row = rows[i]
            var newRows = rows
            newRows.remove(at: i)

            let remainingSquare = selectMagicSquare(rows: newRows, x: x + 1, leftTarget: leftTarget - row[x], rightTarget: rightTarget - row[row.count - x - 1])
            if (remainingSquare != nil) {
                return [row] + remainingSquare!
            }
        }

        // No magic square can be made with these rows, so bail out.
        return nil
    }

    let target = (n * n * n + n) / 2
    return selectMagicSquare(rows: square, x: 0, leftTarget: target, rightTarget: target)!
}


let squares = input.components(separatedBy: "\n\n").map { $0.components(separatedBy: "\n").map { $0.components(separatedBy: " ").map { Int($0)! } } }
for square in squares {
    print(permuteIntoMagicSquare(square))
}
