//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "squares", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


func verifyMagicSquare(_ square: [Int]) -> Bool {
    let dimension = Int(sqrt(Double(square.count)))

    // We need to check that the sums of the N rows, N columns, and 2 diagonals are the same, so we set up a running sum for each of those sequences:
    // - sums[0..<N]: rows' sums from top to bottom
    // - sums[N..<2N]: columns' sums from left to right
    // - sums[2N]: diagonal from upper left to lower right
    // - sums[2N+1]: diagonal from lower left to upper right
    var sums = Array(repeating: 0, count: 2 * dimension + 2)

    // Run through each number in the square and add it to the appropriate row and column sums
    for row in 0..<dimension {
        for col in 0..<dimension {
            let n = row * dimension + col
            sums[row] += square[n]
            sums[dimension + col] += square[n]
        }
    }

    // Run through the diagonals and add them to the appropriate sums
    for row in 0..<dimension {
        sums[2 * dimension] += square[row * (dimension + 1)]
        sums[2 * dimension + 1] += square[dimension - 1 + row * (dimension - 1)]
    }

    // Check that all the numbers are equal by comparing them all with the first
    return sums.reduce(true) { $0 && $1 == sums[0] }
}


let squares = input.components(separatedBy: "\n\n").map { $0.components(separatedBy: CharacterSet.whitespacesAndNewlines).map { Int($0)! } }
for square in squares {
    print(square, "=>", verifyMagicSquare(square))
}
